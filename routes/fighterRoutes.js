const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res) => {
    res.send('GET fighter page');
});

router.get('/:id', (req, res) => {
    res.send(`GET fighter page: ${req.params.id}`);
});

router.post('/', (req, res) => {
    res.send('POST fighter page');
});

router.put('/:id', (req, res) => {
    res.send(`PUT fighter page: ${req.params.id}`);
});

router.delete('/:id', (req, res) => {
    res.send(`DELETE fighter page: ${req.params.id}`);
});

router.get('*', (req, res) => {
    res.status(404).send('Page not found');
});

module.exports = router;