const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res) => {
    res.send('GET user page');
});

router.get('/:id', (req, res) => {
    res.send(`GET user page: ${req.params.id}`);
});

router.post('/', createUserValid, (req, res) => {
    try {
        let data = UserService.create(req.body)
            .then(res => res.send('User is registered'));
    } catch (err) {
        res.err = err;
        res.sendStatus(400).send('Something went wrong');
    }
});

router.put('/:id', (req, res) => {
    res.send(`PUT user page: ${req.params.id}`);
});

router.delete('/:id', (req, res) => {
    res.send(`DELETE user page: ${req.params.id}`);
});

router.get('*', (req, res) => {
    res.status(404).send('Page not found');
});

module.exports = router;