const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/', (req, res) => {
    res.send('GET fights page');
});

router.get('/:id', (req, res) => {
    res.send(`GET fights page: ${req.params.id}`);
});

router.post('/', (req, res) => {
    res.send('POST fights page');
});

router.put('/:id', (req, res) => {
    res.send(`PUT fights page: ${req.params.id}`);
});

router.delete('/:id', (req, res) => {
    res.send(`DELETE fights page: ${req.params.id}`);
});

router.get('*', (req, res) => {
    res.status(404).send('Page not found');
});

module.exports = router;