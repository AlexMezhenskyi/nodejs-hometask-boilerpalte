const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if(!req.body) return res.sendStatus(400);

    let {firstName, lastName, email, phoneNumber, password} = req.body;

    if (firstName && !firstName.trim()) {
        return 'Invalid firstName';
    } else if (lastName && !lastName.trim()) {
        return 'Invalid lastName';
    } else if (email && !email.trim() && !/gmail.com$/i.test(email)) {
        return 'Invalid email';
    } else if (phoneNumber && !phoneNumber.trim()) {
        return 'Invalid phoneNumber';
    } else if (password && !password.trim() && password.length <= 3) {
        return 'Invalid password';
    } else {
        next();
    }

    throw new Error(' Error');
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;